#ifndef PUZZLE_H
#define PUZZLE_H

#include <array>

#include "PuzzleNode.h"

class Puzzle
{
public:
	Puzzle() = default;
	void read_initial_state(const std::string& file_name);
	void solve_puzzle();
	void write_output(const std::string& file_name) const;

private:
	static std::array<PuzzleNode, 2> m_goal_states;
	PuzzleNode m_initial_state;
	PuzzleNode m_reachable_goal;
	PuzzleNode m_result_state;

	// utility function
	void select_target_goal();
};

#endif // !PUZZLE_H

