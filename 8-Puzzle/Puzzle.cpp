// includes for I/O
#include <fstream>

// includes for containers
#include <vector>
#include <queue>

// includes for methods
#include <functional>
#include <algorithm>
#include <iterator>

// user includes
#include "Puzzle.h"

// initialize the possible goal states
std::array<PuzzleNode, 2> Puzzle::m_goal_states = { PuzzleNode(" 12345678"), PuzzleNode("1238 4765") };

void Puzzle::read_initial_state(const std::string& file_name)
{
	std::fstream fs;
	fs.open(file_name, std::ios::in);

	std::string temp;
	std::string grid_layout;

	while (std::getline(fs, temp)) 
		m_initial_state.grid_layout += temp;
	this->select_target_goal();
}

void Puzzle::solve_puzzle()
{
	std::priority_queue<PuzzleNode, std::vector<PuzzleNode>, std::greater<>> node_priority_queue;
	std::vector<PuzzleNode> visited_nodes;

	node_priority_queue.push(m_initial_state);
	while (!node_priority_queue.empty()) {
		PuzzleNode current_node = node_priority_queue.top();

		if (current_node == m_reachable_goal) {
			m_result_state = current_node;
			return;
		}

		node_priority_queue.pop();
		std::vector<PuzzleNode> successors = current_node.generate_successors(m_reachable_goal);

		// for each unique successor, push that successor into the priority queue,
		// add the successor to visited_nodes
		for (auto& successor : successors) {
			if (successor.is_unique(visited_nodes)) {
				node_priority_queue.push(successor);
				visited_nodes.push_back(successor);
			}
		}
	}
}

void Puzzle::write_output(const std::string& file_name) const
{
	std::fstream fs;

	fs.open(file_name, std::ios::out);

	// path_nodes contain nodes from m_result_state to the initial state.
	// the nodes are iteratively added, where the next node is initialized
	// by the parent_node of the current node.
	std::vector<PuzzleNode> path_nodes;
	PuzzleNode current_node = m_result_state;
	while (!current_node.parent_node.empty()) {
		path_nodes.push_back(current_node);
		current_node = current_node.parent_node[0];
	}
	path_nodes.push_back(m_initial_state);
	// reverse the vector so that the first element is m_initial_state
	// and the last element is m_result_state.
	std::reverse(path_nodes.begin(), path_nodes.end());

	// no. of steps = path_nodes.size() - 1, 
	// since path_nodes contains the initial state, which is not
	// a part of the steps.
	fs << "Number of steps required: " << path_nodes.size() - 1 << "\n";
	fs << "Steps: " << "\n\n";
	for (auto& node : path_nodes) {
		fs << node << std::endl;
		
		fs << " |\n"
		   << " ˅\n";
	}
	fs << "GOAL!";
}

void Puzzle::select_target_goal()
{
	unsigned tile_count = 0;

	for (size_t i = 0; i != m_initial_state.grid_layout.size(); ++i) {
		// skip calculation for empty space
		if (m_initial_state.grid_layout[i] == ' ') continue;

		// increment tile_count for each tile with a smaller weight than current tile
		for (size_t j = i + 1; j != m_initial_state.grid_layout.size(); ++j) {
			if (m_initial_state.grid_layout[i] - '0' < m_initial_state.grid_layout[j] - '0')
				++tile_count;
		}
	}

	/* if tile_count is odd, only possible goal is:
	 * 1 2 3
	 * 8   4
	 * 7 6 5
	 *
	 * if tile_count is even, only possible goal is:
	 *   1 2
	 * 3 4 5
	 * 6 7 8
	*/
	if (tile_count % 2) m_reachable_goal = m_goal_states[1];
	else m_reachable_goal = m_goal_states[0];
}
