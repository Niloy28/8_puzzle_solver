#ifndef PUZZLE_NODE_H 
#define PUZZLE_NODE_H

#include <string>
#include <vector>

enum direction { north = -3, south = 3, east = 1, west = -1 };

struct PuzzleNode
{
	// stores the possible sliding directions for each square on the 3x3 grid
	static std::vector<std::vector<direction>> possible_directions;

	std::string grid_layout;
	unsigned distance_from_source = 0;
	unsigned distance_from_goal = 0;
	std::vector<PuzzleNode> parent_node;

	// constructors
	PuzzleNode() = default;
	explicit PuzzleNode(const std::string& grid_layout);
	explicit PuzzleNode(const std::string& grid_layout, unsigned distance_from_source);
	explicit PuzzleNode(const std::string& grid_layout, unsigned distance_from_source, unsigned distance_from_goal);
	explicit PuzzleNode(const std::string& grid_layout, unsigned distance_from_source, unsigned distance_from_goal,
	                    const PuzzleNode& parent);
	explicit PuzzleNode(const std::string& grid_layout, unsigned distance_from_source, const PuzzleNode& parent);

	// overloaded operators
	bool operator == (const PuzzleNode& puzzle_node) const;
	bool operator != (const PuzzleNode& puzzle_node) const;
	bool operator > (const PuzzleNode& puzzle_node) const;

	/*
	 * is_unique(): determines whether *this is a previously explored node,
	 *				by comparing *this against visited_nodes container.
	 *
	 *	generate_successors(): generates successor nodes by performing all possible
	 *						   slides on *this. The argument passed is used by
	 *						   calculate_heuristics() called within the function.
	 *						   This function does not use goal_node in any way.
	 */
	bool is_unique(std::vector<PuzzleNode>& visited_nodes) const;
	std::vector<PuzzleNode> generate_successors(const PuzzleNode& goal_node);

	// overloaded I/O operators
	friend std::ostream& operator << (std::ostream& os, const PuzzleNode& puzzle_node);

private:
	// utility functions
	unsigned calculate_heuristic(const PuzzleNode& goal_node) const;
};

std::ostream& operator << (std::ostream& os, const PuzzleNode& puzzle_node);

#endif