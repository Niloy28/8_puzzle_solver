#include <iostream>

#include <chrono>
#include <string>

#include "Puzzle.h"

int main()
{
	Puzzle puzzle;

	std::string file_name;

	std::cout << "Enter input file name: ";
	std::cin >> file_name;

	puzzle.read_initial_state(file_name);
	std::cout << "Running search: ";
	const auto clock_start = std::chrono::high_resolution_clock::now();
	puzzle.solve_puzzle();
	const auto clock_end = std::chrono::high_resolution_clock::now();

	std::cout << "\nEnter output file name: ";
	std::cin >> file_name;
	puzzle.write_output(file_name);
	std::cout << "Result written in " << file_name << std::endl;

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(clock_end - clock_start);
	std::cout << "Time required for search: " << duration.count() << " milliseconds.";

	return 0;
}
