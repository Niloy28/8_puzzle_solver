#include <sstream>
#include <cstdlib>

#include "PuzzleNode.h"

// initialize possible sliding directions for each square on the 3x3 grid
std::vector<std::vector<direction>> PuzzleNode::possible_directions = { {east, south},        {east, south, west},        {south, west},
																		{north, east, south}, {north, east, south, west}, {north, south, west},
																		{north, east},        {north, east, west},        {north, west} };

PuzzleNode::PuzzleNode(const std::string& grid_layout)
{
	this->grid_layout = grid_layout;
}

PuzzleNode::PuzzleNode(const std::string& grid_layout, const unsigned distance_from_source)
{
	this->grid_layout = grid_layout;
	this->distance_from_source = distance_from_source;
}

PuzzleNode::PuzzleNode(const std::string& grid_layout, const unsigned distance_from_source, const unsigned distance_from_goal)
{
	this->grid_layout = grid_layout;
	this->distance_from_source = distance_from_source;
	this->distance_from_goal = distance_from_goal;
}

PuzzleNode::PuzzleNode(const std::string& grid_layout, const unsigned distance_from_source, const unsigned distance_from_goal,
                       const PuzzleNode& parent)
{
	this->grid_layout = grid_layout;
	this->distance_from_source = distance_from_source;
	this->distance_from_goal = distance_from_goal;
	this->parent_node.push_back(parent);
}

PuzzleNode::PuzzleNode(const std::string& grid_layout, const unsigned distance_from_source, const PuzzleNode& parent)
{
	this->grid_layout = grid_layout;
	this->distance_from_source = distance_from_source;
	this->distance_from_goal = distance_from_goal;
	this->parent_node.push_back(parent);
}

bool PuzzleNode::operator == (const PuzzleNode& puzzle_node) const
{
	return this->grid_layout == puzzle_node.grid_layout;
}

bool PuzzleNode::operator != (const PuzzleNode& puzzle_node) const
{
	return this->grid_layout != grid_layout;
}

bool PuzzleNode::operator > (const PuzzleNode& puzzle_node) const
{
	return (this->distance_from_goal + this->distance_from_source) > (puzzle_node.distance_from_goal + puzzle_node.distance_from_source);
}

bool PuzzleNode::is_unique(std::vector<PuzzleNode>& visited_nodes) const
{
	for (auto& node : visited_nodes)
		if (*this == node) return false;

	return true;
}

std::vector<PuzzleNode> PuzzleNode::generate_successors(const PuzzleNode& goal_node)
{
	std::vector<PuzzleNode> successors;

	// determine possible directions for " " to move
	std::vector<direction> directions_for_gap;
	unsigned gap_index = 0;
	for (unsigned i = 0; i != grid_layout.size(); ++i) {
		if (grid_layout[i] == ' ') {
			gap_index = i;
			directions_for_gap = possible_directions[i];
			break;
		}
	}
	// generate successor grids by moving " " in available directions
	for (auto& direction : directions_for_gap) {
		std::string grid_layout = this->grid_layout;

		switch (direction) {
		case north:
			std::swap(grid_layout[gap_index], grid_layout[gap_index + north]);
			successors.emplace_back(grid_layout, distance_from_source + 1, *this);
			break;
		case south:
			std::swap(grid_layout[gap_index], grid_layout[gap_index + south]);
			successors.emplace_back(grid_layout, distance_from_source + 1, *this);
			break;
		case east:
			std::swap(grid_layout[gap_index], grid_layout[gap_index + east]);
			successors.emplace_back(grid_layout, distance_from_source + 1, *this);
			break;
		case west:
			std::swap(grid_layout[gap_index], grid_layout[gap_index + west]);
			successors.emplace_back(grid_layout, distance_from_source + 1, *this);
			break;
		default: break;
		}
	}

	for(auto &successor : successors) {
		successor.distance_from_goal = successor.calculate_heuristic(goal_node);
	}

	return successors;
}

unsigned PuzzleNode::calculate_heuristic(const PuzzleNode& goal_node) const
{
	unsigned heuristic_distance = 0;
	unsigned misplaced_tile_count = 0;

	for (int i = 0; i != this->grid_layout.size(); ++i) {
		bool is_misplaced = false;

		for (int j = 0; j != goal_node.grid_layout.size(); ++j) {
			if (goal_node.grid_layout[i] == this->grid_layout[j]) {
				if (i != j) is_misplaced = true;

				heuristic_distance += std::abs(i - j);
			}
		}
		misplaced_tile_count += static_cast<unsigned>(is_misplaced);
	}

	// 3 is a magical number, greatly optimizes runtime
	return heuristic_distance / 3 + misplaced_tile_count;
}

std::ostream& operator<<(std::ostream& os, const PuzzleNode& puzzle_node)
{
	unsigned i = 0;

	while (i != puzzle_node.grid_layout.size()) {
		auto sub_string = puzzle_node.grid_layout.substr(i, 3);
		i += 3;

		os << sub_string << "\n";
	}

	return os;
}
